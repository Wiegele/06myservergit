import { Product } from "./Product.js";

export class ClientController{
    
    
    public static onClickBtnInit():void{
        let xhttp: XMLHttpRequest = new XMLHttpRequest();
        xhttp.onload = function(){
            let he: HTMLElement | null = document.getElementById("txtMessage");
            if (he != null) {
                he.setAttribute("value", "init done with status: " + xhttp.status + ", " + xhttp.statusText);
            }
        }
        xhttp.onerror = function(){
            let he: HTMLElement | null = document.getElementById("txtMessage");
            if (he != null) {
                he.setAttribute("value", "init failed with status: " + xhttp.status + ", " + xhttp.statusText);
            }
        }
        xhttp.open('POST', '/products/init', true);
        xhttp.send();
        let he: HTMLElement | null = document.getElementById("txtMessage");
            if (he != null) {
                he.setAttribute("value", "init started...");
            }
    }
    public static onClickBtnSearch():void{
        try{
            let strId: string = (<HTMLInputElement>window.document.getElementById("txtProductId")).value;
            if(strId == null || strId.length < 1){
                throw new Error("no product-id given");
            }
            let xhttp: XMLHttpRequest = new XMLHttpRequest();
            xhttp.onload = function(){
                let product: any = JSON.parse(xhttp.responseText);
                document.getElementById("txtProductId")?.setAttribute("value",product.id);
                document.getElementById("txtProductName")?.setAttribute("value",product.name);
                document.getElementById("txtProductCategory")?.setAttribute("value",product.category);
                document.getElementById("txtProductColor")?.setAttribute("value",product.color);
                document.getElementById("txtMessage")?.setAttribute("value","get done with status: " + xhttp.status + ", " + xhttp.responseText);
            }
            xhttp.open('GET', '/products/' + strId, true);
            xhttp.send();
            window.document.getElementById("txtMessage")?.setAttribute("value", "get product with id started...");
        }catch(error){
            document.getElementById("txtProductId")?.setAttribute("value","");
            document.getElementById("txtProductName")?.setAttribute("value","");
            document.getElementById("txtProductCategory")?.setAttribute("value","");
            document.getElementById("txtProductColor")?.setAttribute("value","");
            window.document.getElementById("txtMessage")?.setAttribute("value","error: " + (error as Error).message);
        }
    }
    public static onClickBtnFilter():void{
        let collProducts: any;
        try{
            let strCategory: string = (<HTMLInputElement>window.document.getElementById("txtProductCategory")).value;
            let strColor: string = (<HTMLInputElement>window.document.getElementById("txtProductColor")).value;

            let xhttp: XMLHttpRequest = new XMLHttpRequest();
            xhttp.onload = function(){
                collProducts = JSON.parse(xhttp.responseText);
                document.getElementById("txtMessage")?.setAttribute("value", "get done with status: " + xhttp.status + ", ' of products read: " + collProducts.length);
                ClientController.fillHtmlTableCars(collProducts);
            }
            xhttp.onerror = function(){
                document.getElementById("txtMessage")?.setAttribute("value", "get product failed with status: " + xhttp.status + ", " + xhttp.statusText);
            }
            let url: any = new URL("http://localhost:3000/products");
            url.searchParams.set("category", strCategory);
            url.searchParams.set("color", strColor);
            xhttp.open('GET', url);
            xhttp.send();
            window.document.getElementById("txtMessage")?.setAttribute("value","get product with id started...");
        }catch(error){
            window.document.getElementById("txtMessage")?.setAttribute("value", "error: " + (error as Error).message);
        }
    }
    private static fillHtmlTableCars(collProducts: any):void{
        let htmlTblCars: HTMLTableElement = (<HTMLTableElement>window.document.getElementById("tblProducts"));
        while(htmlTblCars.tBodies[0].rows.length > 0){
            htmlTblCars.tBodies[0].deleteRow(htmlTblCars.tBodies[0].rows.length - 1);
        }
        for(let products of collProducts){
            let row: HTMLTableRowElement = htmlTblCars.tBodies[0].insertRow(-1);
            let cell: HTMLTableDataCellElement = row.insertCell(0);
            cell.appendChild(document.createTextNode(products.id.toString()));
            cell = row.insertCell(1);
            cell.appendChild(document.createTextNode(products.name));
            cell = row.insertCell(2);
            cell.appendChild(document.createTextNode(products.category));
            cell = row.insertCell(3);
            cell.appendChild(document.createTextNode(products.color));
        }
    }
    public static onClickBtnAdd():void{
        try{
            let strName: string = (<HTMLInputElement>window.document.getElementById("txtProductName")).value;
            let strCategory: string = (<HTMLInputElement>window.document.getElementById("txtProductName")).value;
            let strColor: string = (<HTMLInputElement>window.document.getElementById("txtProductName")).value;
            if(strName == null || strName.length < 1){

            }
            let product: Product = new Product(strName, strCategory, strColor);
            let xhttp: XMLHttpRequest = new XMLHttpRequest();
            xhttp.onload = function(){

            }
            xhttp.onerror = function(){

            }
            xhttp.open('POST', '/products/', true);
            xhttp.setRequestHeader("Accept", "application/json");
            xhttp.setRequestHeader("Content-Type", "application/json");
            //xhttp.send(product.toJson());
            window.document.getElementById("txtMessage")?.setAttribute("value", "add started...");
        }catch(error){
            
        }
    }
}

window.addEventListener('load', function (e) {
    let he: HTMLElement | null = document.getElementById('btnInitProduct');
        if (he != null) {
            he.addEventListener('click', ClientController.onClickBtnInit);
        }
});
window.addEventListener('load', function (e) {
    let he: HTMLElement | null = document.getElementById('btnSearchProduct');
        if (he != null) {
            he.addEventListener('click', ClientController.onClickBtnSearch);
        }
});
window.addEventListener('load', function (e) {
    let he: HTMLElement | null = document.getElementById('btnFilterCatCol');
        if (he != null) {
            he.addEventListener('click', ClientController.onClickBtnFilter);
        }
});