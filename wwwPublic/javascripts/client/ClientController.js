import { Product } from "./Product.js";
export class ClientController {
    static onClickBtnInit() {
        let xhttp = new XMLHttpRequest();
        xhttp.onload = function () {
            let he = document.getElementById("txtMessage");
            if (he != null) {
                he.setAttribute("value", "init done with status: " + xhttp.status + ", " + xhttp.statusText);
            }
        };
        xhttp.onerror = function () {
            let he = document.getElementById("txtMessage");
            if (he != null) {
                he.setAttribute("value", "init failed with status: " + xhttp.status + ", " + xhttp.statusText);
            }
        };
        xhttp.open('POST', '/products/init', true);
        xhttp.send();
        let he = document.getElementById("txtMessage");
        if (he != null) {
            he.setAttribute("value", "init started...");
        }
    }
    static onClickBtnSearch() {
        var _a, _b, _c, _d, _e, _f;
        try {
            let strId = window.document.getElementById("txtProductId").value;
            if (strId == null || strId.length < 1) {
                throw new Error("no product-id given");
            }
            let xhttp = new XMLHttpRequest();
            xhttp.onload = function () {
                var _a, _b, _c, _d, _e;
                let product = JSON.parse(xhttp.responseText);
                (_a = document.getElementById("txtProductId")) === null || _a === void 0 ? void 0 : _a.setAttribute("value", product.id);
                (_b = document.getElementById("txtProductName")) === null || _b === void 0 ? void 0 : _b.setAttribute("value", product.name);
                (_c = document.getElementById("txtProductCategory")) === null || _c === void 0 ? void 0 : _c.setAttribute("value", product.category);
                (_d = document.getElementById("txtProductColor")) === null || _d === void 0 ? void 0 : _d.setAttribute("value", product.color);
                (_e = document.getElementById("txtMessage")) === null || _e === void 0 ? void 0 : _e.setAttribute("value", "get done with status: " + xhttp.status + ", " + xhttp.responseText);
            };
            xhttp.open('GET', '/products/' + strId, true);
            xhttp.send();
            (_a = window.document.getElementById("txtMessage")) === null || _a === void 0 ? void 0 : _a.setAttribute("value", "get product with id started...");
        }
        catch (error) {
            (_b = document.getElementById("txtProductId")) === null || _b === void 0 ? void 0 : _b.setAttribute("value", "");
            (_c = document.getElementById("txtProductName")) === null || _c === void 0 ? void 0 : _c.setAttribute("value", "");
            (_d = document.getElementById("txtProductCategory")) === null || _d === void 0 ? void 0 : _d.setAttribute("value", "");
            (_e = document.getElementById("txtProductColor")) === null || _e === void 0 ? void 0 : _e.setAttribute("value", "");
            (_f = window.document.getElementById("txtMessage")) === null || _f === void 0 ? void 0 : _f.setAttribute("value", "error: " + error.message);
        }
    }
    static onClickBtnFilter() {
        var _a, _b;
        let collProducts;
        try {
            let strCategory = window.document.getElementById("txtProductCategory").value;
            let strColor = window.document.getElementById("txtProductColor").value;
            let xhttp = new XMLHttpRequest();
            xhttp.onload = function () {
                var _a;
                collProducts = JSON.parse(xhttp.responseText);
                (_a = document.getElementById("txtMessage")) === null || _a === void 0 ? void 0 : _a.setAttribute("value", "get done with status: " + xhttp.status + ", ' of products read: " + collProducts.length);
                ClientController.fillHtmlTableCars(collProducts);
            };
            xhttp.onerror = function () {
                var _a;
                (_a = document.getElementById("txtMessage")) === null || _a === void 0 ? void 0 : _a.setAttribute("value", "get product failed with status: " + xhttp.status + ", " + xhttp.statusText);
            };
            let url = new URL("http://localhost:3000/products");
            url.searchParams.set("category", strCategory);
            url.searchParams.set("color", strColor);
            xhttp.open('GET', url);
            xhttp.send();
            (_a = window.document.getElementById("txtMessage")) === null || _a === void 0 ? void 0 : _a.setAttribute("value", "get product with id started...");
        }
        catch (error) {
            (_b = window.document.getElementById("txtMessage")) === null || _b === void 0 ? void 0 : _b.setAttribute("value", "error: " + error.message);
        }
    }
    static fillHtmlTableCars(collProducts) {
        let htmlTblCars = window.document.getElementById("tblProducts");
        while (htmlTblCars.tBodies[0].rows.length > 0) {
            htmlTblCars.tBodies[0].deleteRow(htmlTblCars.tBodies[0].rows.length - 1);
        }
        for (let products of collProducts) {
            let row = htmlTblCars.tBodies[0].insertRow(-1);
            let cell = row.insertCell(0);
            cell.appendChild(document.createTextNode(products.id.toString()));
            cell = row.insertCell(1);
            cell.appendChild(document.createTextNode(products.name));
            cell = row.insertCell(2);
            cell.appendChild(document.createTextNode(products.category));
            cell = row.insertCell(3);
            cell.appendChild(document.createTextNode(products.color));
        }
    }
    static onClickBtnAdd() {
        var _a;
        try {
            let strName = window.document.getElementById("txtProductName").value;
            let strCategory = window.document.getElementById("txtProductName").value;
            let strColor = window.document.getElementById("txtProductName").value;
            if (strName == null || strName.length < 1) {
            }
            let product = new Product(strName, strCategory, strColor);
            let xhttp = new XMLHttpRequest();
            xhttp.onload = function () {
            };
            xhttp.onerror = function () {
            };
            xhttp.open('POST', '/products/', true);
            xhttp.setRequestHeader("Accept", "application/json");
            xhttp.setRequestHeader("Content-Type", "application/json");
            //xhttp.send(product.toJson());
            (_a = window.document.getElementById("txtMessage")) === null || _a === void 0 ? void 0 : _a.setAttribute("value", "add started...");
        }
        catch (error) {
        }
    }
}
window.addEventListener('load', function (e) {
    let he = document.getElementById('btnInitProduct');
    if (he != null) {
        he.addEventListener('click', ClientController.onClickBtnInit);
    }
});
window.addEventListener('load', function (e) {
    let he = document.getElementById('btnSearchProduct');
    if (he != null) {
        he.addEventListener('click', ClientController.onClickBtnSearch);
    }
});
window.addEventListener('load', function (e) {
    let he = document.getElementById('btnFilterCatCol');
    if (he != null) {
        he.addEventListener('click', ClientController.onClickBtnFilter);
    }
});
//# sourceMappingURL=ClientController.js.map